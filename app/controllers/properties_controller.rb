class PropertiesController < ApplicationController
  def index
    @properties = Property.where("zip = ?0", params[:zip_code])
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @homes }
    end    
  end
  
  def new
    @properties = Property.all
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @homes }
    end 
  end  
end
