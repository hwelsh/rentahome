class Bathroom < ActiveRecord::Base
  attr_accessible :bathroom
  
  validates(:bathroom, presence: true)
    
  has_many(:properties)
end
