class Pet < ActiveRecord::Base
  attr_accessible :pet
  
  validates(:pet, presence: true)
    
  has_many(:properties)
end
