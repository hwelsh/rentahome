class Property < ActiveRecord::Base
  attr_accessible :city, :state, :county, :zip, :room, :bathroom, :pet
  
  validates(:city, presence: true)
  validates(:state, presence: true)
  validates(:county, presence: true)
  validates(:zip, presence: true)
  validates(:room, presence: true)
  validates(:bathroom, presence: true)
  validates(:pet, presence: true)
  
  belongs_to(:city, :foreign_key => "city")
  belongs_to(:state, :foreign_key => "state")
  belongs_to(:county, :foreign_key => "county")  
  belongs_to(:zip, :foreign_key => "zip")
  belongs_to(:room, :foreign_key => "room")
  belongs_to(:bathroom, :foreign_key => "bathroom")
  belongs_to(:pet, :foreign_key => "pet")
  
end
