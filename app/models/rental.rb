class Rental < ActiveRecord::Base
  attr_accessible :rental_type
  
  validates(:rental_type, presence: true)
  
  has_many(:properties)
end
