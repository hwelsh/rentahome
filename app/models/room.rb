class Room < ActiveRecord::Base
  attr_accessible :room
  
  validates(:room, presence: true)
    
  has_many(:properties)
end
