class State < ActiveRecord::Base
  attr_accessible :name, :abbr
  
  validates(:name, presence: true)
  validates(:abbr, presence: true)
  
  has_many(:properties)
end
  