class Zip < ActiveRecord::Base
  attr_accessible :zip_code
  
  validates(:zip_code, presence: true)
  
  has_many(:properties)
end
