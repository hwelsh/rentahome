class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.integer :rental
      t.string  :price
      t.string  :address
      t.integer :state
      t.integer :city
      t.integer :county
      t.integer :zip
      t.integer :room
      t.integer :bathroom
      t.integer :pet
      
      t.timestamps
    end
  end
end
