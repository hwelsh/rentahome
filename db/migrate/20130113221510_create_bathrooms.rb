class CreateBathrooms < ActiveRecord::Migration
  def change
    create_table :bathrooms do |t|
      t.string :bathroom

      t.timestamps
    end
  end
end
