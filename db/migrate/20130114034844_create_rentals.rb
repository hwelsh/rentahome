class CreateRentals < ActiveRecord::Migration
  def change
    create_table :rentals do |t|
      t.string :rental_type

      t.timestamps
    end
  end
end
